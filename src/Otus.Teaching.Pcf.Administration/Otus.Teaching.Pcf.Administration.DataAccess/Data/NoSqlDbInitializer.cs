﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Repositories.Models;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class NoSqlDbInitializer
     : IDbInitializer
    {
        private readonly IMongoCollection<Employee> _noSqlCollectionEmployee;
        private readonly IMongoCollection<Role> _noSqlCollectionRole;

        public NoSqlDbInitializer(IOptions<AdminStoreNoSqlDBSettigs> adminStoreDatabaseSettings)
        {
            var mongoClient = new MongoClient(
                adminStoreDatabaseSettings.Value.ConnectionString);

            var mongoDatabase = mongoClient.GetDatabase(adminStoreDatabaseSettings.Value.DatabaseName);

            _noSqlCollectionEmployee = mongoDatabase.GetCollection<Employee>(adminStoreDatabaseSettings.Value.BooksCollectionNameEmployee);
            _noSqlCollectionRole = mongoDatabase.GetCollection<Role>(adminStoreDatabaseSettings.Value.BooksCollectionNameRole);

        }

        public void InitializeDb()
        {

            var resE = _noSqlCollectionEmployee.InsertManyAsync(FakeDataFactoryNoSql.Employees);
            var resR = _noSqlCollectionRole.InsertManyAsync(FakeDataFactoryNoSql.Roles);
        }
    }
}
