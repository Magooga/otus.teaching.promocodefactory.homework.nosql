using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;
using SharpCompress.Common;


namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class NoSqlRepository<T> : 
        IRepository<T> 
        where T: BaseEntity
    {
        private readonly IMongoCollection<T> _noSqlCollection;

        public NoSqlRepository(IOptions<AdminStoreNoSqlDBSettigs> adminStoreDatabaseSettings)
        {
            var mongoClient = new MongoClient(
                adminStoreDatabaseSettings.Value.ConnectionString);

            var mongoDatabase = mongoClient.GetDatabase(adminStoreDatabaseSettings.Value.DatabaseName);

            if(typeof(T) == typeof(Employee))
            {
               _noSqlCollection = mongoDatabase.GetCollection<T>(adminStoreDatabaseSettings.Value.BooksCollectionNameEmployee); 
            } else 
            {
                _noSqlCollection = mongoDatabase.GetCollection<T>(adminStoreDatabaseSettings.Value.BooksCollectionNameRole); 
            }

            
        }


        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return  await _noSqlCollection.Find(new BsonDocument()).ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
           return await _noSqlCollection.FindAsync(x => x.Id == id).Result.FirstAsync();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return await _noSqlCollection.Find(x => ids.Contains(x.Id)).ToListAsync();
            //var entities = await _dataContext.Set<T>().Where(x => ids.Contains(x.Id)).ToListAsync();
            //return entities;
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            return await _noSqlCollection.Find(predicate).FirstAsync<T>();
            //return await _dataContext.Set<T>().FirstOrDefaultAsync(predicate);
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return await _noSqlCollection.Find(predicate).ToListAsync<T>();
            //return await _dataContext.Set<T>().Where(predicate).ToListAsync();
        }

        public async Task AddAsync(T entity)
        {

             await _noSqlCollection.InsertOneAsync(entity);

        }

        public async Task UpdateAsync(T entity)
        {

            if (_noSqlCollection.FindAsync(c => c.Id == entity.Id).Result.ToList().Count == 0)
            {
                throw new Exception("Entity not found");
            }

            var o = await _noSqlCollection.Find(c => c.Id == entity.Id).FirstAsync();


            await _noSqlCollection.FindOneAndReplaceAsync(c => c.Id == entity.Id, entity);
        }

        public async Task DeleteAsync(T entity)
        {
            var record = await _noSqlCollection.DeleteOneAsync(
                Builders<T>.Filter.Where(rec => rec.Id == entity.Id)
                );
        }
    }
}