namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories.Models
{
    public class DBResponse
    {
        public string? Id { get; set; }
        public string? ResultText { get; set; }
        public string? Code { get; set; }

    }

}
