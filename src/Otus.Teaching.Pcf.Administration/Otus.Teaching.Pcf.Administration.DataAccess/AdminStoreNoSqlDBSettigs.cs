namespace Otus.Teaching.Pcf.Administration.DataAccess;

public class AdminStoreNoSqlDBSettigs
{
        public string ConnectionString { get; set; } = null!;

        public string DatabaseName { get; set; } = null!;

        public string BooksCollectionNameEmployee { get; set; } = null!;

        public string BooksCollectionNameRole { get; set; } = null!;
}
