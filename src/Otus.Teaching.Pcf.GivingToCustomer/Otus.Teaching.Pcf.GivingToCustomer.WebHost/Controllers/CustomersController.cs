﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
//using Newtonsoft.Json;
using System.Text.Json;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IDistributedCache _cache;

        public CustomersController(IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository, IDistributedCache cache)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _cache = cache;
        }
        
        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers =  await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return Ok(response);
        }
        
        /// <summary>
        /// Получить клиента по id
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer =  await _customerRepository.GetByIdAsync(id);

            var response = new CustomerResponse(customer);

            return Ok(response);
        }
        
        /// <summary>
        /// Создать нового клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            List<Preference> preferences = new List<Preference>();

            // Get Prefences from cache
            foreach(var pID in request.PreferenceIds)
            {
                // пытаемся получить данные из кэша по id
                var prefString = await _cache.GetStringAsync(pID.ToString());

                if (prefString != null) 
                    preferences.Add(JsonSerializer.Deserialize<Preference>(prefString));
                else
                    break;
            }

            //Получаем предпочтения из бд и сохраняем большой объект
            //var preferences = await _preferenceRepository
            if(preferences.Count == 0)
            {
                preferences = (List<Preference>) await _preferenceRepository
                    .GetRangeByIdsAsync(request.PreferenceIds);

                if(preferences.Count > 0)
                {
                    foreach(var pref in preferences)
                    {
                        Console.WriteLine($"{pref.Name} извлечен из базы данных");
                        // сериализуем данные в строку в формате json
                        var prefString = JsonSerializer.Serialize(pref);
                        // сохраняем строковое представление объекта в формате json в кэш на 30 минуты
                        await _cache.SetStringAsync(
                            key: pref.Id.ToString(), 
                            value: prefString, 
                            options: new DistributedCacheEntryOptions
                            {
                                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(30)
                            });
                    }
                }    
            }


            Customer customer = CustomerMapper.MapFromModel(request, preferences);
            
            await _customerRepository.AddAsync(customer);

            return CreatedAtAction(nameof(GetCustomerAsync), new {id = customer.Id}, customer.Id);
        }
        
        /// <summary>
        /// Обновить клиента
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        /// <param name="request">Данные запроса></param>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            List<Preference> preferences = new List<Preference>();

            var customer = await _customerRepository.GetByIdAsync(id);
            
            if (customer == null)
                return NotFound();
            

            // Get Prefences from cache
            foreach(var pID in request.PreferenceIds)
            {
                // пытаемся получить данные из кэша по id
                var prefString = await _cache.GetStringAsync(pID.ToString());

                if (prefString != null) 
                    preferences.Add(JsonSerializer.Deserialize<Preference>(prefString));
                else
                    break;
            }

            //var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            if(preferences.Count == 0)
            {
                preferences = (List<Preference>) await _preferenceRepository
                    .GetRangeByIdsAsync(request.PreferenceIds);

                if(preferences.Count > 0)
                {
                    foreach(var pref in preferences)
                    {
                        Console.WriteLine($"{pref.Name} извлечен из базы данных");
                        // сериализуем данные в строку в формате json
                        var prefString = JsonSerializer.Serialize(pref);
                        // сохраняем строковое представление объекта в формате json в кэш на 30 минуты
                        await _cache.SetStringAsync(
                            key: pref.Id.ToString(), 
                            value: prefString, 
                            options: new DistributedCacheEntryOptions
                            {
                                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(30)
                            });
                    }
                }    
            }

            
            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return NoContent();
        }
        
        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            
            if (customer == null)
                return NotFound();

            await _customerRepository.DeleteAsync(customer);

            return NoContent();
        }
    }
}